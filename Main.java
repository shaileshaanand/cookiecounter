/*Some Imports- BufferedReader, InputStreamReader & IOException for command line
input. ArrayList & HashSet for storing varoius data and Iterator for iterating
over data:*/
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
//Declaring the main class and variables
public class Main {
    public static void main(String[] args)throws IOException {
        BufferedReader in =new BufferedReader(new InputStreamReader(System.in));
        int cookie[][]=new int[5][5];
        ArrayList<Relation> relations=new ArrayList<>();
        Relation firstRelation;
        Index firstIndex,secondIndex,index1,index2,current;
        Relation relation;
        HashSet<Index> chocoChip;
        HashSet<Index> explored;
        ArrayList<HashSet<Index>> chocoChips=new ArrayList<>();
        int i,j;
        String row;
        int totalNumberOfOne=0;
        //now we take user input row by row in the nested loop
        for (i=0;i<5;i++) {
            System.out.println("Enter the Row "+(i+1)+" (Eg. 10110)");
            row=in.readLine();
            for (j = 0; j < 5; j++) {
                cookie[i][j]=Character.getNumericValue(row.charAt(j));
            }
        }
        //A multipurpose loop to disply the data, count 1's and make all the relations
        for (i=0;i<5;i++) {
            for (j=0;j<5;j++) {
                if (cookie[i][j]==1) {
                    totalNumberOfOne++; //counting 1's
                    if (j!=4 && cookie[i][j+1]==1) { // Check if below element is 1
                        relations.add(new Relation(new Index(i,j),new Index(i,j+1)));
                    }
                    if (i!=4 && cookie[i+1][j]==1) { // Check if right element is 1
                        relations.add(new Relation(new Index(i,j),new Index(i+1,j)));
                    }
                }
                System.out.print(cookie[i][j]+" "); //display the entered cookie
            }
            System.out.println();
        }
        /*
        Now we have the relations so we go through them to form choco chips
        frontier is used to store the indices to be explored(i.e. the neighbors)
        explored is used to keep track of the already added 1's
        */
        while (!relations.isEmpty()) {
            chocoChip = new HashSet<>();
            explored = new HashSet<>();
            firstRelation = relations.get(0);
            firstIndex = firstRelation.getIndex1();
            secondIndex = firstRelation.getIndex2();
            ArrayList<Index> frontier = new ArrayList<>();
            //the first relation has to be a part of the chip so we add them
            //and since they have to be explored, we add them to frontier too
            chocoChip.add(firstIndex);
            chocoChip.add(secondIndex);
            frontier.add(firstIndex);
            frontier.add(secondIndex);
            while (!frontier.isEmpty()) {
                //in this loop we explore the frontier till its empty
                current = frontier.get(0);
                frontier.remove(0);
                if (!explored.contains(current)) {
                    chocoChip.add(current);
                    explored.add(current);
                }
                Iterator<Relation> it = relations.iterator();
                while (it.hasNext()) {
                    //this loop finds the neighbors and puts them in frontier
                    //if not already explored or in frontier
                    relation = it.next();
                    index1 = relation.getIndex1();
                    index2 = relation.getIndex2();
                    if (index1.equals(current) || index2.equals(current)) {
                        /* if the current element in the frontier has a relation,
                           it is in the choco chip so we add it to frontier where
                           it is explored for more neighbors and added to choco chip */
                        if (!(frontier.contains(index1) || explored.contains(index1))) {
                            frontier.add(index1);
                        }
                        if (!(frontier.contains(index2) || explored.contains(index2))) {
                            frontier.add(index2);
                        } else {
                            // remove the relation from relations because it is no longer required
                            // as both the relevent indices are already explored
                            it.remove();
                        }
                    }
                }
            }
            //our choco chip is complete here so we add it to chocoChips
            chocoChips.add(chocoChip);
        }
        //chocoChips are ready so now we do the output formatting
        String result="[";
        /* Count the no. of 1's from the chocoChips and subtract it from
        total 1's calculated earlier to get single isolated 1's */
        int totalChocoChipLength=0;
        for (HashSet<Index> item:chocoChips) {
            result+=item.size()+",";
            totalChocoChipLength+=item.size();
        }
        int chocoChipsRemaining=totalNumberOfOne-totalChocoChipLength;
        /* add 1's to the result chocoChipsRemaining times to account for
        isolated 1's */
        for (i=1;i<=chocoChipsRemaining;i++) {
            result+="1,";
        }
        /* finally the output but it has a nasty ',' (comma) at the end so we
        remove it by substring() */
        System.out.println("\n"+result.substring(0,result.length()-1)+"]");
    }
}

