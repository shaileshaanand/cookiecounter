// The Relation class represemts a relation between 2 indices
public class Relation {
    private final Index index1;
    private final Index index2;
    public Relation(Index index1, Index index2) {
        this.index1 = index1;
        this.index2 = index2;
    }
    public Index getIndex1() {
        return index1;
    }
    public Index getIndex2() {
        return index2;
    }
}

