//The Index class represents an index or location of element in 2d array
public class Index {
    private final int row;
    private final int column;
    public Index(int row, int column) {
        this.row = row;
        this.column = column;
    }
    public int getRow() {
        return row;
    }
    public int getColumn() {
        return column;
    }
    @Override
    public boolean equals(Object o) {
        if (this==o) {
            return true;
        }
        Index index=(Index)o;
        if (this.row==index.getRow() && this.column==index.getColumn()) {
            return true;
        }
        return false;
    }
    @Override
    public int hashCode() {
        return (" "+row+column).hashCode();
    }
}

